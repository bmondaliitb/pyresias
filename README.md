# Pyresias

<h1>A toy parton shower for educational purposes.</h1>

<h2>Pre-requisites:</h2>
<ol>
<li>LHAPDF 6.5.4 with the python interface built in.</li>
<li>python 3.8.10</li>
<li>matplotlib, numpy, scipy.</li>
</ol>
(tested on these but should work on newer versions)

<h2>Usage:</h2>
python pyresias.py -n [Number of Branches] -Q [Starting scale] -c
[Cutoff Scale] -o [outputdirectory] -d [enable debugging output]
